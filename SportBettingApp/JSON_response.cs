﻿using System;

public class JSON_response
{
    public string Code { get; set; }
    public string Result { get; set; }
    public string Key { get; set; }
    public string Password { get; set; }
    public string SuccessfulLogin { get; set; }
    public string TextMessage { get; set; }
    public string Token { get; set; }
    public string Url { get; set; }
    public string Username { get; set; }
}
