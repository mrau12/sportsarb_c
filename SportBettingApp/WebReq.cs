﻿using System;
using System.Net;
using System.IO;


    public class WebReq
    {
        private HttpWebRequest request;
        private HttpWebResponse response;
        private Stream receiveStream;
        private StreamReader readStream;
        private Logger log = new Logger();

        // Login web request
        public WebReq(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = WebRequestMethods.Http.Get;
            request.Accept = "application/json; charset=utf-8";
            this.response = (HttpWebResponse)request.GetResponse();
        }

        // Register web request
        public WebReq(string url, JSON_response json_obj)
        {            
            this.request = (HttpWebRequest)WebRequest.Create(url);
            request.Headers["AOKey"] = json_obj.Key;
            request.Headers["AOToken"] = json_obj.Token;
            this.response = (HttpWebResponse)request.GetResponse();
        }

        public string GetStream()
        {
            try {
                this.receiveStream = this.response.GetResponseStream();
                this.readStream = new StreamReader(receiveStream);
                return readStream.ReadLine();
                }
            catch
            {
                log.Error("Exception error in .GetStream() for WebReq");
                throw;
            }
            finally
            {
                if (readStream != null)
                {
                    readStream.Close();
                }
            }

        }
               
    }

