﻿using System;
using System.Net;
using System.IO;

namespace SportBettingApp
{
    class Program
    {
        static void Main(string[] args)
        {
            JSON_response json_object = new JSON_response();
            ARequest req = new ARequest(json_object);
            req.SetKeyTokenAndUrl(req.Login());
            req.SetCodeAndMsg(req.Register());
            req.ConsoleOut();
        }
    }
}
