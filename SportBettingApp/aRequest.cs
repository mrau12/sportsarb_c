﻿using System;
using System.Net;
using System.IO;

public class ARequest
{
    String user;
    String passw;
    JSON_response json_obj;

    public ARequest(JSON_response json_obj)
    {
        user = "webapiuser93";
        passw = MD5Hash("QS1f0ybd");
        this.json_obj = json_obj;
    }

    private String getUser()
    {
        return user;
    }

    private String getPassword()
    {
        return passw;
    }

    public static string MD5Hash(string input)
    {
        System.Security.Cryptography.MD5CryptoServiceProvider x = new System.Security.Cryptography.MD5CryptoServiceProvider();
        byte[] bs = System.Text.Encoding.UTF8.GetBytes(input);
        bs = x.ComputeHash(bs);
        System.Text.StringBuilder s = new System.Text.StringBuilder();
        foreach (byte b in bs)
        {
            s.Append(b.ToString("x2").ToLower());
        }
        return s.ToString();
    }
       
    public Newtonsoft.Json.Linq.JObject Login()
    {
        string url = string.Format("https://webapi.asianodds88.com/AsianOddsService/Login?username={0}&password={1}", getUser(), getPassword());
        WebReq wb_login = new WebReq(url);
        return Newtonsoft.Json.Linq.JObject.Parse(wb_login.GetStream());
    }

    public void SetKeyTokenAndUrl(Newtonsoft.Json.Linq.JObject newtonObj)
    {
         json_obj.Key = (string)newtonObj["Result"]["Key"];
         json_obj.Token = (string)newtonObj["Result"]["Token"];
         json_obj.Url = (string)newtonObj["Result"]["Url"];
    }

    public Newtonsoft.Json.Linq.JObject Register()
    {
        string url_reg = json_obj.Url + string.Format("/Register?username={0}", getUser());
        WebReq wb = new WebReq(url_reg, json_obj);
        return Newtonsoft.Json.Linq.JObject.Parse(wb.GetStream());
    }


    public void SetCodeAndMsg(Newtonsoft.Json.Linq.JObject newtonObj)
    {
        json_obj.Code = (string)newtonObj["Code"];
        json_obj.TextMessage = (string)newtonObj["Result"]["TextMessage"];
    }

    public void ConsoleOut()
    {
        Console.WriteLine(json_obj.Code);
        Console.WriteLine(json_obj.TextMessage);
        Console.ReadLine();
    }       
}

